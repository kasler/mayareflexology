if (!process.env.NODE_ENV){
  console.log('No NODE_ENV variable detected, setting to `development`');
  process.env.NODE_ENV = 'development';
}

console.log('Running gulp with NODE_ENV set to', process.env.NODE_ENV)

require('harmonize')(['harmony_default_parameters']);
require('dotenv').config({silent: true});
const gulp = require('gulp'),
    scss = require('gulp-sass'),
    gutil = require('gulp-util'),
    babel = require('babelify'),
    sourcemaps = require('gulp-sourcemaps'),
    watch = require('gulp-watch'),
    browserSync = require('browser-sync').create(),
    fs = require('fs'),
    autoprefixer = require('autoprefixer'),
    postcss = require('gulp-postcss'),
    cleanCss = require('gulp-clean-css'),
    uglify = require('gulp-uglify'),
    gulpif = require('gulp-if'),
    plumber = require('gulp-plumber'),
    notify = require('gulp-notify'),
    envify = require('envify'),
    browserify = require('browserify'),
    watchify = require('watchify'),
    buffer = require('vinyl-buffer'),
    source = require('vinyl-source-stream'),
    map = require('vinyl-map'),
    marked = require('marked');

// Config marked
marked.setOptions({
  breaks: true,
});

// We change marked's <a> to open links in new tab
var renderer = new marked.Renderer();

renderer.link = function(href, title, text) {
  if (this.options.sanitize) {
    try {
      var prot = decodeURIComponent(unescape(href))
        .replace(/[^\w:]/g, '')
        .toLowerCase();
    } catch (e) {
      return '';
    }
    if (prot.indexOf('javascript:') === 0 || prot.indexOf('vbscript:') === 0) {
      return '';
    }
  }
  //         Here's the change
  //                v
  var out = '<a target="_blank" href="' + href + '"';
  if (title) {
    out += ' title="' + title + '"';
  }
  out += '>' + text + '</a>';
  return out;
};

// Set config
var config = {
  scssPath: "./assets/stylesheets",
  jsPath: "./assets/javascript/client",
  imagesPath: "./assets/images",
  staticHtmlPath: "./assets/javascript/html",
  outputDir: "./public",
  enableSourcemaps: (process.env.ENABLE_SOURCEMAPS === "true"),
};

// Aggregates fonts from different sources and puts them in the public dir
gulp.task('fonts',function(){
  gulp.src('./node_modules/font-awesome/fonts/**.*')
    .pipe(plumber())
    .pipe(gulp.dest(`${config.outputDir}/fonts`))
});

gulp.task('scss', function(){
  const manifestPath = `${config.scssPath}/application.scss`;

  fs.access(manifestPath, fs.R_OK, (err,data) => {
    if(err){
      gutil.log(err);
    }else{
      gulp.src(manifestPath)
        .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
        .pipe(gulpif(config.enableSourcemaps, sourcemaps.init()))
        .pipe(scss({
          includePaths: [
            './node_modules/bootstrap-sass/assets/stylesheets',
            './node_modules/font-awesome/scss',
            './node_modules/bootstrap-material-design/sass',
          ],
          style: 'compact'
        }))
        .pipe(postcss([ autoprefixer({ browsers: ['last 10 versions'] }) ]))
        .pipe(cleanCss())
        .pipe(gulpif(config.enableSourcemaps, sourcemaps.write('./maps')))
        .pipe(gulp.dest(config.outputDir))
        .pipe(browserSync.stream())
        .pipe(notify({message:'Finished CSS task', onLast: true}))
    }
  });
});

gulp.task('images', function(){
  gulp.src(`${config.imagesPath}/**/*.{jpg,png,gif,svg}`)
    .pipe(plumber())
    .pipe(gulp.dest(`${config.outputDir}/images`))
});


gulp.task('static-html',()=>{
  gulp.src(`${config.staticHtmlPath}/**/*.*`)
    .pipe(plumber())
    .pipe(gulp.dest(`${config.outputDir}/html`))
});

gulp.task('js:app',()=>{
  return bundle(`${config.jsPath}/index.js`, config.outputDir, 'application.js', false);
});

gulp.task('watch', function() {
  bundle(`${config.jsPath}/index.js`, config.outputDir, 'application.js', true);
  gulp.watch('./.env', ['build']);
  gulp.watch(`${config.scssPath}/**/*.scss`,['scss']);
});



gulp.task('browsersync', function(){
  browserSync.init({
    proxy: "localhost:5000"
  });
});

gulp.task('start', ['images', 'fonts', 'scss', 'js:app','static-html']);
gulp.task('build', ['images', 'fonts', 'scss', 'js:app','static-html']);
gulp.task('default', ['build', 'browsersync', 'watch']);

// Helper function to bundle js using browserify
function bundle(sourcePath, targetPath, targetFile, watch){

  // Create browserify instance
  var options = {
    cache: {},
    packageCache: {},
    entries: [sourcePath],
    debug: config.enableSourcemaps
  };

  // We want to watch the files only if the `watch` param was specified
  var b = watch ? watchify(browserify(options)) : browserify(options);

  // Set transormations
  b.transform(babel, {
    presets: ["es2015", "react"],
    plugins: ["transform-object-rest-spread"]
  })
   .transform(envify);

  // Set log messages
  b.on('log', (message) => {
    gutil.log("Browserify bundler: " + message);
  });

  function rebundle(){

    return b.bundle()
    .on('error', (err) => {
      console.error(err.stack, err.message); // Log any errors
    })
    .pipe(source(targetFile)) // Wrap browserify's stream in a gulp stream
    .pipe(buffer()) // Turn the content stream into a buffer type
    .pipe(plumber())
    .pipe(gulpif(config.enableSourcemaps, sourcemaps.init({loadMaps: true})))
    .pipe(uglify({mangle: !config.enableSourcemaps}))
    .pipe(gulpif(config.enableSourcemaps, sourcemaps.write('./maps')))
    .pipe(gulp.dest(targetPath))
    .pipe(browserSync.stream())
  }
  b.on('update', rebundle);
  return rebundle();
}
