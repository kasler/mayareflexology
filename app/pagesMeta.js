const _ = require('lodash');
console.log('========================= linkMeta start =====================');
const getMetaHash = () => {
    return {
      "/":{
        title:"רפלקסולוגיה בעיר - טיפולי רפלקסולוגיה מאת מאיה צוק",
        //keywords:"accelerometer, arduino, arduino uno, board, code, connect sensor to controller, microcontroller, piezo, potentiometer, sensors, servo,schematic",
        description: "טיפולי רפלקסולוגיה בתל אביב מאת הרפלקסולוגית מאיה צוק"
      },
    }
  }

module.exports = {
  getLinkMeta : (compJson,req) => {
    var path,linkMeta,pathMetaHash;
    path = req.path.toString();
    linkMeta = {};
    pathMetaHash = getMetaHash()[path];
    linkMeta[path] = pathMetaHash;
    console.log('req.query.components')
    console.log(req.query.components)
    //check is this a magic link we need to dynamiclly generate page meta for.
    if( !_.isEmpty(req.query) && !_.isEmpty(req.query.components) ){
      var board,compList,compJson,boardLongName,compListStr,numericName,compNumericNameList,compNumericNameListStr;
      compList = [];
      compNumericNameList = [];
      compShortName = [];
      compJson = JSON.parse(req.app.locals.componentsJSON);
      var components = JSON.parse("[" +req.query.components + "]");
      _.each(components,(compId) => {
          if( !_.isUndefined(compJson[compId]) &&  !_.isUndefined(compJson[compId].name) ){
            if(!_.isUndefined(compJson[compId].type)){
              if(compJson[compId].type === "controllers"){
                board = compJson[compId].metaData.short;
                boardLongName = compJson[compId].name;
              }else if(compJson[compId].type != "powerSupplies"){
                compList.push(compJson[compId].name);
                if(compJson[compId].metaData.numericName){
                  numericName = compJson[compId].metaData.numericName;
                }else{
                  numericName = (compJson[compId].metaData.numeric) ? compJson[compId].metaData.numeric : compJson[compId].name;
                }
                if(compJson[compId].metaData.short){
                  compShortName.push(compJson[compId].metaData.short);
                }else{
                  compShortName.push(numericName);
                }
                compNumericNameList.push(numericName)
              }
            }
          }
      });

      compListStr = compList.join(', ');
      compShortNameListStr = compShortName.join(', ');
      compNumericNameListStr = compNumericNameList.join(', ');
      linkMeta[path] = {
        title : 'How to wire' + ' ' + compListStr + ' to ' + ' ' + board,
        //keywords : compListStr + ', ' + board,
        //description :'Make a Circuito Project with ' + compListStr + 'and a' + ' ' + board
        description: `Learn how to wire the ${compShortNameListStr} to ${board} in a few simple steps. The primary components for this circuit are: ${boardLongName} and ${compNumericNameListStr}. Drag and drop these components onto the canvas, and instantly get a list of secondary parts, wiring instructions and a test code for your circuit. Try it for free.`

      };
    }

    return linkMeta;
  }
}
