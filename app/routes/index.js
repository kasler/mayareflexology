const express = require('express'),
      _ = require('lodash'),
      router = express.Router(),
      config = require('config'),
      uuid = require('uuid'),
      Prismic = require('prismic-javascript'),
      jsdom = require("jsdom");
const { JSDOM } = jsdom;
const nodemailer = require("nodemailer");
const pagesMeta = require("../pagesMeta");
const sm = require('sitemap');
const smJson = require('../../sitemap.json');
const reactAppRoutes = ['/','/stylesheet']

const defaultOpts = { cookie:true, ignoreMethods: ['HEAD', 'OPTIONS']};

process.on('unhandledRejection', (err) => {
  console.log('Unhandlefd Rejection: ' + err);
});

//set sitemap
const mainSitemap = sm.createSitemap(smJson);
const sitemap = sm.buildSitemapIndex({
   urls:['https://www.circuito.io/sitemap_circuito.xml','https://www.circuito.io/blog/sitemap_index.xml']
 })

const prismicRouter = require('./prismic')(router);

// setup email data with unicode symbols
let mailOptions = {
  from: '"Maya reflexology" <maya@example.com>', // sender address
  to: "theshaklawist@gmail.com, maya@walla.com", // list of receivers
  subject: "לקוח חדש! 👻", // Subject line
  text: "Hello world?", // plain text body
};

let transporter = null;
nodemailer.createTestAccount((err, account)=> {
  transporter = nodemailer.createTransport({
    host: "smtp.ethereal.email",
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: account.user, // generated ethereal user
      pass: account.pass // generated ethereal password
    }
  });
});

router.post('/send_mail', (req, res)=> {
  console.log(req);
  if (!req.body.name || !req.body.phone) {
    console.log("ERROR no name or phone");
    res.status(500).end();
    return;
  }

  mailOptions.text = `שם: ${req.body.name}\nטלפון: ${req.body.phone}`;
  transporter.sendMail(mailOptions)
  .then((info)=> {
    console.log("Message sent: %s", info.messageId);
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    res.status(200).end();
  })
  .catch((err)=> {
    console.log("ERROR - " + err);
    res.status(500).end();
  });
});

router.get('/sitemap.xml', function(req, res) {
  mainSitemap.toXML( function (err, xml) {
      if (err) {
        return res.status(500).end();
      }
      res.header('Content-Type', 'application/xml');
      res.send( xml );
  });
});

// Shows the main form always
// this decleration should ALWAYS be last
router.get('*', (req,res) => {
  console.log("HEREEEE");
  let LinkMeta = pagesMeta.getLinkMeta(req.app.locals.componentsJSON,req);
  let params = { meta : LinkMeta, path: req.path};
  res.render('index',{params});
});

module.exports = router;
