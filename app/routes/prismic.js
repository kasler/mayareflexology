const mcache = require('memory-cache');
const PrismicConfig = require('../prismic-configuration');
const Cookies = require('cookies');
const Prismic = require('prismic-javascript');

module.exports = function(router){
  var cacheTime = 1500; //1800 / 60 = 30minutes
  var cache = (duration) => {
    return (req, res, next) => {
      let key = '__express__' + req.originalUrl || req.url
      let cachedBody = mcache.get(key)
      if (cachedBody) {
        res.send(cachedBody)
        return
      } else {
        res.sendResponse = res.send
        res.send = (body) => {
          mcache.put(key, body, duration * 1000);
          res.sendResponse(body)
        }
        next()
      }
    }
  }
  /*
   * -------------- Routes --------------
   */

  /**
  * Preconfigured prismic preview
  */

   router.get('/preview', (req, res) => {
     const token = req.query.token;
     if (token) {
       req.prismic.api.previewSession(token, PrismicConfig.linkResolver, '/').then((url) => {
         const cookies = new Cookies(req, res);
         cookies.set(Prismic.previewCookie, token, { maxAge: 30 * 60 * 1000, path: '/', httpOnly: false });
         res.redirect(302, url);
       }).catch((err) => {
         res.status(500).send(`Error 500 in preview: ${err.message}`);
       });
     } else {
       res.send(400, 'Missing token from querystring');
     }
   });

  /**
  * Route for blog homepage
  */
  router.get('/blog/related/:type',cache(cacheTime), (req, res) => {
        var Predicates = [Prismic.Predicates.at("document.tags", ["homepage"])];

        // Query the posts
        req.prismic.api.query(
          Prismic.Predicates.any("document.type", ["blog_post"])
        ).then(function(response){
          res.setHeader('Cache-Control', 'public, max-age=' + cacheTime);
          res.setHeader('Content-Type', 'application/json');
          res.send(JSON.stringify(response));
        });
  });

  /**
  * Route for blog homepage
  */
  router.get(['/blog','/blog.amp'],[cache(cacheTime)], (req, res) => {
    res.setHeader('Cache-Control', 'public, max-age=' + cacheTime);

    console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1");

    var host = req.get('host');
    var fullUrl = 'https://' + host + req.originalUrl;
    var paginationIntrvl = 6;
    var nextPage = (req.query.p) ? (parseInt(req.query.p) + 1) : 2;
    var urlAmpIndx = fullUrl.indexOf('.amp')
    if(urlAmpIndx !== -1){
        fullUrl = fullUrl.replace('.amp','');
    }
    // Query the homepage
    req.prismic.api.getSingle("bloghome").then((bloghome) => {
      console.log(bloghome)
      // If a document is returned...
      if (bloghome) {
        const formattedType = {"blog_post":"Blog post"};
        const queryOptions = {
          pageSize:100,
          page : 1,
          orderings: '[my.blog_post.date desc]'
        };

        var Predicates = [Prismic.Predicates.at("document.tags", ["homepage"])];
        if(req.query.q){
          Predicates.push([Prismic.Predicates.fulltext('document',encodeURIComponent(req.query.q) )]);
        }
        // Query the posts
        req.prismic.api.query(
            Predicates,
            queryOptions
        ).then(function(response) {
          var types = [];
          var containsObject = function(obj, list) {
              var i;
              for (i = 0; i < list.length; i++) {
                  if (list[i].value === obj) {
                      return true;
                  }
              }
              return false;
          }
          response.results.forEach( (item,indx) =>{
            item.formattedType = formattedType[item.type];
            if(!containsObject(item.type,types)){
              types.push({
                value:item.type,
                name:formattedType[item.type]
              });
            }
          });

          var meta = {
            description:(bloghome.data.meta_description) ? bloghome.data.meta_description : '',
            canonical:fullUrl,
            //segmentKey:process.env.SEGMENT_CLIENT_API_KEY,
          }
          //console.log(process.env.SEGMENT_CLIENT_API_KEY);

          var templateFile = 'prismic/bloghome';

          if(req.originalUrl.indexOf('.amp') != -1){
              templateFile = 'prismic/bloghome_amp.pug';
          }

          // console.log(response.results)
          console.log("response.results.length:::::::::::::")
          console.log(response.results.length)
          console.log("meta");
          console.log(meta);
          // Render the blog homepage
          res.render(templateFile, {
            bloghome,
            meta,
            posts : response.results,
            types: types,
            query:(req.query.q) ? req.query.q : "",
            footerBnr:false,
            nextPage,
          });
        });

      } else {
        // If a bloghome document is not returned, give an error
        res.statusCode = 404;
        let params = { meta : {}, path: req.path , csrfToken:'', noIndex: true }
        res.render('index',{ params });
      }
    })
  });


  /**
  * Route for blog posts
  */
  router.get(['/blog/:uid','/blog/:uid.amp']/*,[cache(cacheTime)]*/,(req, res) => {
    //res.setHeader('Cache-Control', 'public, max-age=' + cacheTime);
    var postType = (req.path.indexOf('/blog/') != -1) ? 'blog_post' : 'other_post';

    let host = (req.get('host').indexOf('www') != -1) ? req.get('host') : 'www.' + req.get('host');
    let fullUrl = 'https://' + host + req.originalUrl;
    var urlAmpIndx = fullUrl.indexOf('.amp')
    if(urlAmpIndx !== -1){
        fullUrl = fullUrl.replace('.amp','');
    }

    // Define the uid from the url
    const isAmp = (req.originalUrl.indexOf('.amp') != -1);
    const uid = (isAmp) ? req.params.uid.split('.')[0] : req.params.uid;
    console.log(uid)
    // Query the post by its uid
    req.prismic.api.getByUID(postType, uid).then(post => {
      console.log('got here !!!!')
      console.log(post.data.body)
      if (post) {
        // If a document is returned, render the post
        var pageCanonical = post.data.canonical_url;
        if(pageCanonical){
          fullUrl = pageCanonical;
        }
        //console.log("process.env.SEGMENT_CLIENT_API_KEY");
        //console.log(process.env.SEGMENT_CLIENT_API_KEY);
        var metaData = {
          description:(post.data.meta_description) ? post.data.meta_description : '',
          pageTitle:(post.data.page_title) ? post.data.page_title : post.data.title,
          canonical:fullUrl,
          coverImg:(post.data.cover_image) ? post.data.cover_image : ( (post.data.gallery_image_one) ? post.data.gallery_image_one : null),
          //segmentKey:process.env.SEGMENT_CLIENT_API_KEY,
        }

        if(metaData.coverImg){    
          metaData.coverImg.url = metaData.coverImg.url.replace('https://','http://');    
        }

        if(isAmp){
          console.log('IS AMP !!!!')
          res.render('prismic/pages/'+postType+'_amp.pug', { post:post ,footerBnr:true, meta:metaData });
        }else{
          res.render('prismic/pages/'+postType, { post:post ,footerBnr:true, meta:metaData });
        }

      // Else give an error
      } else {
        res.statusCode = 404;
        let params = { meta : {}, path: req.path , csrfToken:'', noIndex: true }
        res.render('index',{params});
      }
    });
  });
}
