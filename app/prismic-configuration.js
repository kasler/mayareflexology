module.exports = {

  apiEndpoint: 'https://reflexology.cdn.prismic.io/api/v2',

  // -- Access token if the Master is not open
  // accessToken: 'xxxxxx',

  // OAuth
  // clientId: 'xxxxxx',
  // clientSecret: 'xxxxxx',

  // -- Links resolution rules
  // This function will be used to generate links to Prismic.io documents
  // As your project grows, you should update this function according to your routes
  linkResolver(doc) {
    console.log('doc::::::::::::::')
    console.log(doc)
    if (doc.type == 'bloghome') {
      return '/blog';
    }
    if (doc.type == 'blog_post') {
      return '/blog/' + encodeURIComponent(doc.uid);
    }

    return '/'
  }
};
