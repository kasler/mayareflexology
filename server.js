"use strict";

// Initialize libs
const express = require('express');
const port = process.env.PORT || 3000;
const ip = process.env.BIND_IP || '0.0.0.0';
//const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
//const favicon = require('serve-favicon');
const logger = require('morgan');
const path = require('path');
const contentFilter = require('content-filter');
const enforce = require('express-sslify');
const compression = require('compression');
const robots = require('robots.txt');
const mainRoutes = require('./app/routes');
const Prismic = require('prismic-javascript');
const PrismicDOM = require('prismic-dom');
const PrismicConfig = require('./app/prismic-configuration');
const UIhelpers = require('./assets/javascript/js/prismic');

// Initialize express
const app = express();

app.all('*', function(req, res, next){
  res.removeHeader("X-Powered-By");
  res.header('Referrer-Policy' ,'origin');
  res.header('Access-Control-Allow-Origin', process.env.FOXYCART_URL);
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

// Connect to DB. Start listening once a connection was opened
//utils.connect()
  //.on('reconnected', console.log)
  //.once('open', listen)
  //.on('error', (err) => {
  //  console.error(err);
  //  setTimeout(utils.connect, 5000);
  //});

//ROBOTS TXT configurations
// Pass in the absolute path to your robots.txt file
app.use(robots(__dirname + '/robots.txt'))

// Express configuration
app.use(logger(process.env.LOG_LEVEL || "tiny"));
app.use(compression()); // compress all requestsex

// Force ssl in production
if(app.get('env') === 'production'){
  app.use(enforce.HTTPS({ trustProtoHeader: true }))
}

app.use('/static', express.static('public'));
app.use('/static_private', express.static('private'));
app.use(bodyParser.urlencoded({ extended: true })); // BodyParser parses requests
app.use(contentFilter()); // filters our content from NoSql injections
app.use(bodyParser.json());
//app.use(favicon(__dirname + '/public/images/favicon.png'));

// Middleware to connect to inject prismic context
app.use((req, res, next) => {
  res.locals.ctx = {
    endpoint: PrismicConfig.apiEndpoint,
    linkResolver: PrismicConfig.linkResolver,
  };

  // Add UI helpers to access them in templates
  res.locals.UIhelpers = UIhelpers;

  // Add PrismicDOM in locals to access them in templates
  res.locals.PrismicDOM = PrismicDOM;

  // Add the prismic.io API to the req
  Prismic.api(PrismicConfig.apiEndpoint, {
    accessToken: PrismicConfig.accessToken,
    req,
  }).then((api) => {
    req.prismic = { api };
    next();
  }).catch((error) => {
    next(error.message);
  });
});

app.set('view engine', 'pug'); // We use pug as our templating engine
app.set('views', path.join(process.cwd(), 'app', 'views'));
app.set('mailDir', path.join(app.get('views'), 'mails'));

// Dev specific config
if (app.get('env') === 'development') {
  app.locals.pretty = true;
}


// We are using a wildcard inside the main route file
// that's why it should be mounted last
app.use('/', mainRoutes);

// Error middleware (should be last)
app.use((err, req, res, next) => {
  console.error(err.stack);
  next(err);
})

// Start listening
function listen(){
  console.log("HEREEEE2");
  app.listen(port, ip,function(){
    console.log('App listening at %s port %s (%s)', ip, port, app.get("env"));
  });
}

listen();

module.exports = app;
