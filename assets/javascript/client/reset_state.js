import { RESET_STATE } from './reducers';

export default function(){
  return({
    type: RESET_STATE
  })
}
