import createFetch from 'fetch-ponyfill';
import { REQUEST_SUBMIT_TIMEOUT } from './config';

const {fetch} = createFetch();
const metaToken = document.head.querySelector("meta[name='csrf-token']");
const csrfToken = (metaToken) ? metaToken.getAttribute('content') : null;
const reqOptions = {
  credentials: 'same-origin', // <-- includes cookies in the request
  headers: {
    'CSRF-Token': csrfToken // <-- is the csrf token as a header
  }
};

export const sendMail = (name, phone) => {
  return _setTimeout(
    fetch('/send_mail',{
      method: 'POST',
      credentials: 'same-origin', // <-- includes cookies in the request
      headers: {
        'CSRF-Token': csrfToken, // <-- is the csrf token as a header
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ name, phone })
    }), REQUEST_SUBMIT_TIMEOUT)
  .then((res) => {
    return _checkFetchStatus(res);
  });
}

// Privates
function _setTimeout(promise, timeout = null){
  return new Promise((resolve, reject)=>{

    // Set a timeout for the promise
    if(timeout){
      setTimeout(()=>{
        var error = new Error(`Request timeout: request exceeded ${timeout} ms`);
        error.response = {status: 503, statusText: "timeout"};
        reject(error);
      }, timeout);
    }

    // The resolve method will be called after fetch is finished,
    // After that, the timedout reject will not work
    promise.then(resolve, reject);/* TODO weird timeouts(result)=>{ // That's on resolve
      clearTimeout(timeoutId);
      resolve(result);
    }, (error)=>{ // That's on reject
      clearTimeout(timeoutId);
      reject(error);
    });*/
  })
}

function _checkFetchStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  } else {
    var error = new Error(response.statusText);
    error.response = response;
    throw error;
  }
}
