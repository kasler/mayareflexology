import React from 'react';

const Btn = (({
  label,
  onClick,
  isEnabled,
  type,
  onMouseLeave,
  icon,
  className,
  iconClassName,
  id,
  children,
  isStretch,
})=>{
  let typography = {
    "primary":"text-app-btn",
    "secondary":"text-app-btn-2"
  };
  let fontStyleClass = (type.indexOf("primary") !== -1) ? typography.primary : typography.secondary;
  let iconElement = icon ? <i className={'material-icons icon ' + iconClassName}>{icon}</i> : null;
  let btnLabel = (label)  ? <span>{label}</span> : null;
  let stretchBtnClass = (isStretch) ? "btn-stretch" : " ";
  return(
    <button className={`robo-btn ${stretchBtnClass} ${className} ${type} ${fontStyleClass}`} id={id}
            disabled={!isEnabled}
            onMouseLeave={onMouseLeave}
            onClick={onClick}>
      {iconElement}
      {btnLabel}
      {children}
    </button>);
});

Btn.displayName = "Button";
Btn.propTypes = {
  label: React.PropTypes.string.isRequired,
  onClick: React.PropTypes.func,
  onMouseLeave: React.PropTypes.func,
  isEnabled: React.PropTypes.bool,
  isStretch: React.PropTypes.bool,
  type: React.PropTypes.string,
  icon: React.PropTypes.string,
  className: React.PropTypes.string,
  iconClassName: React.PropTypes.string,
  id: React.PropTypes.string,
  children: React.PropTypes.element,
}
Btn.defaultProps = {
  type: 'primary',
  isEnabled: true,
  className: '',
  iconClassName: '',
}

export default Btn;
