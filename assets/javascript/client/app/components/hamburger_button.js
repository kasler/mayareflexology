import React from 'react';

const HamburgerButton = (({onClick, className})=>{
  return(
    <a href="javascript: void(0);" className={"hamburger btn " + className} onClick={onClick}>
    	<i className='material-icons icon'>menu</i>
    </a>
  );
});

HamburgerButton.displayName = "HamburgerButton";
HamburgerButton.propTypes = {
  onClick: React.PropTypes.func,
  className: React.PropTypes.string,
}

HamburgerButton.defaultProps = {
  className: ''
}

export default HamburgerButton;
