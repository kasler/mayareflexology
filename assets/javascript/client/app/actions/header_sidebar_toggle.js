/**
 * Created by roboamir on 25/07/16.
 */
import { SIDEBAR_TOGGLE } from '../constants';

export default function(){
  return{
    type: SIDEBAR_TOGGLE
  }
}

