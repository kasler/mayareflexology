/**
 * Created by roboamir on 20/07/23.
 */
import React from 'react';
import {Link} from 'react-router';
import MediaQuery from 'react-responsive';
import { ON_MOBILE_MQ, ON_DESKTOP_MQ } from '../../config';
import HamburgerButton from '../components/hamburger_button'
import {sendMail} from '../../api';

class Homepage extends React.Component {
  constructor() {
    super();

    this.state = { isHamburgerOpen: false, name: '', phone: '' };

    this._getHeaderNav = this._getHeaderNav.bind(this);
    this._renderHeader = this._renderHeader.bind(this);

    this.directionsRef = null;
    this.contactRef = null;
    this.aboutMeRef = null;
    this.upperOffset = 0;
  }

  componentDidMount() {
    document.body.classList.add('home-page'); // FOR VIDEO MODAL

    this.scrollableContent.addEventListener('scroll', this._handleScroll);
    
    if (window.location.href.includes('directions')) {
      setTimeout(()=> window.scrollTo(0,document.body.scrollHeight), 1000);
    }
  }

  _renderWalkthroughItem(image, text) {
    return(
      <div className='homepage-walkthrough'>
        <img className='homepage-walkthrough-image' src={image}/>
        <div className='homepage-walkthrough-text text-homepage-body-1-dark'>{text}</div>
      </div>
    );
  }

  _renderOrderNowBtn(isTop) {
    return(
      <button className="robo-btn primary go-to-app" onClick={()=> window.scrollTo(0, this.contactRef.offsetTop + this.upperOffset)}>
        קבע\י פגישה
      </button>
    );
  }

  _renderPost(posts, i) {
    if (posts == null || posts.length <= i) {
      return null;
    }

    let image = (posts[i].type === 'component_post') ? posts[i].data.gallery_image_one.url : posts[i].data.cover_image.url;
    let link = (posts[i].type === 'component_post') ? '/component/' + posts[i].uid : '/blog/' + posts[i].uid;
    let text = posts[i].data.page_title;
    let postImgStyle = {"backgroundImage":`url(${image})`}
    return(
      <a className='homepage-post-container blog_post' href={link + "/"} target="_blank" rel="nofollow">
          <div className='homepage-post-image' style={postImgStyle}></div>
          <h4 className="text-homepage-sub-head-1">{text}</h4>
      </a>
    );
  }

  _renderPostDummy(image, text, link) {
    let postImgStyle = {"backgroundImage":`url(/static/images/${image}`}
    return(
      <a className='homepage-post-container blog_post' href={link + "/"} target="_blank" rel="nofollow">
          <div className='homepage-post-image' style={postImgStyle}></div>
          <h4 className="text-homepage-sub-head-1">{text}</h4>
      </a>
    );
  }

  _getHeaderNav() {
    return ([
      <a className='header-links menu' href="https://maya-reflexology.herokuapp.com" target="_blank"
        rel="nofollow">
        ראשי
      </a>,
      <div className='header-links menu' onClick={()=> { window.scrollTo(0, this.contactRef.offsetTop + this.upperOffset); this.setState({isHamburgerOpen: false});}}>
        פנה\י אלינו
      </div>,
      <div className='header-links menu' onClick={()=> { window.scrollTo(0, this.aboutMeRef.offsetTop + this.upperOffset); this.setState({isHamburgerOpen: false});}}>
        קצת על עצמי
      </div>,
      <div className='header-links menu' onClick={()=> { window.scrollTo(0, this.directionsRef.offsetTop + this.upperOffset); this.setState({isHamburgerOpen: false});}}>
        הוראות הגעה
      </div>,
      <a className='header-links menu' href="/blog/benefits">
        עוד על רפלקסולוגיה
      </a>
    ]);
  }

  _renderHeader(isLight, isFixed) {
    let headerTextColor = 'light';
    if (isLight) {
      headerTextColor = 'dark';
    }

    return(
      <div className={`homepage-header ${isLight ? 'light' : 'dark'} ${isFixed ? 'fixed' : ''}`} ref={(elem)=> this.headerElem = elem}>
        <MediaQuery query={ON_MOBILE_MQ}>
          <HamburgerButton onClick={()=>
            this.setState({isHamburgerOpen: !this.state.isHamburgerOpen})}
          />
        </MediaQuery>
        <MediaQuery query={ON_DESKTOP_MQ}>
          <div className='header-links-container'>
            {this._getHeaderNav()}
          </div>
        </MediaQuery>
        <a href="https://facebook.com/mayatzukreflexology" className="facebook">
          <img src="/static/images/facebook.svg"/>
        </a>
      </div>
    );
  }

  render() {
    return(
      <div className='homepage-container' ref={(elem)=> this.scrollableContent = elem}>
        {this._renderHeader(this.state.isLightHeader, true)}
        <div className='hamburger-container'>
          <MediaQuery query={ON_MOBILE_MQ}>
            <div className='hamburger-container-inner'>
              {
                this.state.isHamburgerOpen ?
                  this._getHeaderNav().map((elem)=> {console.log("WOW"); return (<div className='hamburger-elem'>{elem}</div>);})
                : null
              }
            </div>
          </MediaQuery>
        </div>
        <div className='upper-container' ref={(elem)=> {if (elem) this.upperOffset = elem.offsetHeight}}>
          <div className="upper-bg"/>
          <nav style={{"width": "100%"}}>
            <div className='upper-container-content'>
              <h1>
                רפלקסולוגיה בעיר
              </h1>
              <p>טיפולי רפלקסולוגיה בת"א, מאת מאיה צוק</p>
              {this._renderOrderNowBtn(true)}
            </div>
          </nav>
        </div>
        <table>
          <tr>
            <div className='inner-container-seperator'>
              <div className="text-homepage-header-1">
              ...רוגע, שחרור ושלווה
              </div>
            </div>
          </tr>
          <tr>
            <div className='inner-container'>
              <div className='row space-around mobile-rev-row'>
                <div className='column'>
                  <h2>
                  שיטה טבעית לטיפול דרך כפות הרגליים...
                  </h2>
                  <div className='text-homepage-body-1-light'>
                     על פי הרפלקסולוגיה, איברי הגוף כמו גם מצב הנפש והקוגניציה<br/>
                     משתקפים דרך כפות הרגליים. באמצעות טכניקות שונות של עיסוי ולחיצות<br/>
                     ניתן לטפל במגוון רחב של מצבים בריאותיים:
                  </div>
                  <ul>
                    <li>בעיות מערכת הנשימה</li>
                    <li>טיפול  בסטרס</li>
                    <li>כאבי גב ושלד</li>
                    <li>כאבי ראש ומיגרנות</li>
                    <li>נשים במעגל הפוריות</li>
                    <li>בעיות במערכת העיכול</li>
                    <li>הפרעות קשב וריכוז</li>
                    <li>מחלות אוטואימוניות</li>
                  </ul>
                </div>
                <div className='image-column'
                        style={{backgroundImage: "url(/static/images/ref2.jpg)"}}
                />
              </div>
            </div>
          </tr>
          <tr>
            <div className='inner-container thin'>
              <div className='text-homepage-body-1-dark'>
                "מי שיש לו בריאות, יש לו תקווה.ומי שיש לו תקווה, יש לו הכל..."
              </div>
              <div className="go-to-app-wrapper">
                {this._renderOrderNowBtn(true)}
              </div>
            </div>
          </tr>
          <tr>
            <div className='inner-container'>
              <div className='row space-around'>
                <div className='image-column'
                      style={{backgroundImage: "url(/static/images/house.png)"}}
                />
                <div className='column'>
                  <h2>
                    מה כולל טיפול?
                  </h2>
                  <div className='text-homepage-body-1-light'>
                    תהליך הריפוי מתאים אישית לכל מטופל ומטופל.<br/>
                    הטיפול מתחיל באבחון קפדני של כפות הרגליים ותשאול קצר<br/>
                    המתייחס לבעיית המטופל.<br/>
                    לאחר מכן, נכין תכנית טיפולים הנדרשת לפתרון הבעייה, כמו גם הגורמים לה<br/>
                    ונמשיך לטיפול דרך לחיצות ועיסוי כפות הרגליים בטכניקות ברפלקסולוגיה.<br/>
                  </div>
                  <div className='text-homepage-body-1-light bold'>
                    הטיפול מתבצע בקליניקה, באווירה רגועה ונעימה במיוחד,<br/>
                    או בבית הלקוח באיזור תל אביב. משך הטיפול 45 דק' \ שעה שלמה<br/>
                  </div>
                </div>
              </div>
            </div>
          </tr>
          <tr>
            <div id='me' className='inner-container' ref={ (ref) => this.aboutMeRef=ref}>
              <div className='row space-around mobile-rev-row'>
                <div className='column'>
                  <h2>
                    קצת על עצמי...
                  </h2>
                  <div className='text-homepage-body-1-light'>
                    שמי מאיה צוק, רפלקסולוגית בכירה.<br/>
                    אני מזמינה אתכם אלי לקליניקה<br/>
                    למדתי כל מיני דברים במשך שנתיים.<br/>
                    תחילת דרכי בלה בלה בלה<br/>
                    ונמשיך לטיפול של חצי שעה \ 45 דקות דרך כפות הרגליים.
                  </div>
                </div>
                <div className='image-column'
                        style={{backgroundImage: "url(/static/images/maya.jpg)"}}
                />
              </div>
            </div>
          </tr>
          <tr>
            <div id='contact' className='inner-container' ref={ (ref) => this.contactRef=ref}>
              <h2 className='contact'>
                לקביעת תור ומידע נוסף התקשר ל 0526444719
              </h2>
              <p>
                או מלא\י פרטים ונחזור אליך:
              </p>
              <div className='details'>
                <input placeholder='שם מלא' value={this.state.name} onChange={(e)=> this.setState({name: e.target.value})}/>
                <input placeholder='טלפון' value={this.state.phone} onChange={(e)=> this.setState({phone: e.target.value})}/>
                <button className={"robo-btn primary go-to-app "
                        + (this.state.name === '' || this.state.phone === '' ? 'disabled' : '')}
                        onClick={()=>sendMail(this.state.name, this.state.phone).then(()=>alert('תודה רבה!'))}>
                  שלח
                </button>
              </div>
              <br/>
              <p>
                50% הנחה לטיפול הראשון :)
              </p>
            </div>
          </tr>
          <tr>
            <div className='inner-container'>
              <h2>
                עוד על רפלקסולוגיה...
              </h2>
              <nav className='baseline'>
                {this._renderPostDummy('ref1.jpg', 'פוסט מהבלוג', '/')}
                {this._renderPostDummy('ref2.jpg', 'פוסט מהבלוג', '/')}
                {this._renderPostDummy('ref1.jpg', 'פוסט מהבלוג', '/')}
              </nav>
            </div>
          </tr>
          <tr>
            <div id='directions' className='inner-container' ref={ (ref) => this.directionsRef=ref}>
              <div className='row space-between mobile-rev-row'>
                <a className='image-column map'
                    href='https://www.google.com/maps/place/Nahalat+Binyamin+St+87,+Tel+Aviv-Yafo/@32.062016,34.7583399,15z/data=!4m5!3m4!1s0x151d4c9da2782ff7:0x46d088058c2a0f40!8m2!3d32.0608886!4d34.7727165'
                />
                <div className='column'>
                  <h2>
                    אז איך מגיעים?
                  </h2>
                  <div className='text-homepage-body-1-light'>
                    הטיפולים מתבצעים בכתובת<br/> נחלת בנימין 87, תל אביב<br/>
                    ניתן להזמין גם טיפולים בבית הלקוח באיזור תל אביב
                  </div>
                </div>
              </div>
            </div>
          </tr>
        </table>
      </div>
    );
  }
}

Homepage.displayName = 'Homepage';

export default Homepage;
