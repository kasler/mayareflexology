import { LOCATION_CHANGE } from 'react-router-redux';
import * as types from './constants';

window.isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent);

const initialState = {
  showSEOTitle:true,
  isSidebarOpen: false,
}

export default function(state = initialState, action){
  switch(action.type){
    case LOCATION_CHANGE:
      return({
        ...state,
        isSidebarOpen: false,
        showSEOTitle:( (action.payload.search === state.searchQuery || state.searchQuery === null) ? true : false ),
        searchQuery:action.payload.search,
      })
    case types.HEADER_VIDEO_OPEN:
      return({
        ...state,
        headerModalIsOpen: true
      });
    case types.HEADER_VIDEO_CLOSE:
      return({
        ...state,
        headerModalIsOpen: false
      });
    case types.SIDEBAR_TOGGLE:
      return({
        ...state,
        isSidebarOpen: !state.isSidebarOpen
      });
    case types.SIDEBAR_CLOSE:
      return({
        ...state,
        isSidebarOpen: false
      });
    default:
      return({
        ...state,
      });
  }
}
