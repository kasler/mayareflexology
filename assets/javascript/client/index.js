import 'babel-polyfill';
import 'document-register-element' // ES2015 //polyfill for document.registerElement()
import jQuery from 'jquery';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux'
import configureStore from './configure_store';
import StyleSheetApp from './app/containers/stylesheet_app';
import Homepage from './app/containers/homepage';

// Set jquery as global, this is needed for bootstrap
window.jQuery = window.$ = jQuery;

require('bootstrap-sass');
require('bootstrap-material-design');

// Create a redux store
let store = configureStore({});

// Init material design plugin
$.material.init();

// Get containers
let mainContainer = document.getElementById('main-app');

// Use Redux's store history
const history = syncHistoryWithStore(browserHistory, store);


/**

Routing Important disclaimer!

--------------------------------------------
All routes will point to 404 pages
unless they are both added in the react app & also
in the Express routes/index.js file.
--------------------------------------------

Routing Important disclaimer!

**/

let MainApp = () => {
  return(
    <Provider store={store}>
    <Router history={history} >
        <Route path='/' component={Homepage}/>
        <Route path='stylesheet' component={StyleSheetApp}/>
      </Router>
    </Provider>
  );
}

// Inital react render
render(
  <MainApp />,
  mainContainer
);