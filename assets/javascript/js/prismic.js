var PrismicDOM = require('prismic-dom');
var Elements = PrismicDOM.RichText.Elements;
const month_names_short = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

function getAmpLink(fullUrl){
  var ampUrl = null;
  if(fullUrl[fullUrl.length - 1] === "/"){
    ampUrl = fullUrl.substring(0,fullUrl.length - 1) + ".amp/";
  }else{
    ampUrl = fullUrl + ".amp/";
  }
  return ampUrl;
}

function getPostDate(post){
    if (post.data.publication_date && post.data.publication_date != null){
      return post.data.publication_date;
    }else{
      var d = new Date(post.first_publication_date);
      return d.toDateString();
    }
}
// Function to Style the date
function getDate(post) {
  var date = new Date(post.data.date);
  if (date) {
    var suffix = '';
    switch (date.getUTCDate().toString().substr(date.getUTCDate().toString().length - 1)) {
    case '1':
      suffix = 'st';
      break;
    case '2':
      suffix = 'nd';
      break;
    case '3':
      suffix = 'rd';
      break;
    default :
      suffix = 'th';
    }

    var formatedDate = month_names_short[date.getUTCMonth()] + ' ' +
            date.getUTCDate() + suffix + ', ' + date.getUTCFullYear();
    return formatedDate;
  } else {
    return '';
  }
}

// Function to get the first paragraph of a post
function getFirstParagraph(post) {
  const slices = (post.data.body) ? post.data.body : post.data.intro_text;
  let firstParagraph = '';
  let haveFirstParagraph = false;
  //console.log(post.data)
  slices.forEach(function(slice) {
    if (!haveFirstParagraph) {
      if (slice.slice_type == "text") {
        slice.primary.text.forEach(function(block){
          if (block.type == "paragraph") {
            if (!haveFirstParagraph) {
              firstParagraph += block.text;
              haveFirstParagraph = true;
            }
          }
        });
      }
    }
  });
  return firstParagraph;
}

function getSocialLink(post,pageURL,provider){
  var link = null;
  switch (provider) {
    case "facebook":
        link = "https://www.facebook.com/sharer.php?u=" + pageURL;
        break;
    case "twitter":
        link = "https://twitter.com/intent/tweet?url=" + pageURL;
        break;
    case "linkedin":
        link = "https://www.linkedin.com/shareArticle?mini=true&url=" + pageURL;
        break;
    case "pinterest":
        link = "https://pinterest.com/pin/create/button/?url=" + pageURL;
  }
  return link;
}

var pugHtmlSerializer = function (type, element, content, children) {
  console.log('pugHtmlSerializer')
  /**
  Example YouTube JSON
  { type: 'video',
    16:59:06 web.1         |       embed_url: 'https://youtu.be/pOqiqRHrAHE',
    16:59:06 web.1         |       title: 'Homemade Game Console- "NinTIMdo RP"',
    16:59:06 web.1         |       provider_name: 'YouTube',
    16:59:06 web.1         |       thumbnail_url: 'https://i.ytimg.com/vi/pOqiqRHrAHE/hqdefault.jpg',
    16:59:06 web.1         |       width: 480,
    16:59:06 web.1         |       height: 270,
    16:59:06 web.1         |       version: '1.0',
    16:59:06 web.1         |       author_name: 'TimEE',
    16:59:06 web.1         |       author_url: 'https://www.youtube.com/channel/UCzIave08v9rjqpg3tA8RugQ',
    16:59:06 web.1         |       provider_url: 'https://www.youtube.com/',
    16:59:06 web.1         |       cache_age: null,
    16:59:06 web.1         |       thumbnail_width: 480,
  }
  */
  var serializeEmbed = function(element) {
    function extractUrlValue(key, url){
      var match = url.match('[?&]' + key + '=([^&]+)');
      return match ? match[1] : null;
    }

    var fullUrl = element.oembed.embed_url;
    var isYouTube = ( fullUrl.indexOf('youtube.com') != -1 );
    var vidUrlSplit =  fullUrl.split('/');
    var vidId = vidUrlSplit[vidUrlSplit.length - 1];
    if(vidId.indexOf('&') !== -1){
      var vidUrl =  fullUrl.substr(fullUrl.indexOf('v=') + 2,fullUrl.length);
      var vidId = (vidUrl.indexOf('&') === -1) ? vidUrl : (vidUrl.split('&')[0]);  
    }
    if(vidId.indexOf('watch?v=') !== -1){
      var vidId = extractUrlValue('v',fullUrl);
    }

    return (`
      <div class="embed">
          <amp-youtube layout="responsive" width="${element.oembed.width}" height="${element.oembed.height}" data-videoid="${vidId}"> </amp-youtube>
      </div>
    `);
  }
  switch(type) {

    // Add a class to paragraph elements
    case Elements.paragraph:
      return '<p class="paragraph-class">' + children.join('') + '</p>';

    // Don't wrap images in a <p> tag
    case Elements.image:
       console.log(element)
       //return (element.alt) ? '<amp-img layout="responsive" src="' + element.url + '" alt="' + element.alt + '" title="' + element.alt + '"/>' : '<amp-img layout="responsive" src="' + element.url + '"/>';
       return '<amp-img layout="responsive" src="' + element.url + '" width="' + element.dimensions.width +'" height="'+ element.dimensions.height +'"></amp-img>';
       // dimensions: { width: 921, height: 550 },
    case Elements.embed:
       console.log("embed element:::::::")
       console.log(element)
       return serializeEmbed(element)
    	
    case Elements.hyperlink:
      var target = 'target="_blank" rel="noopener"';
      var linkUrl = element.data.url;
      return '<a class="some-link"' + target + ' href="' + linkUrl + '">' + content + '</a>';

    // Return null to stick with the default behavior
    default:
      return null;
  }
};

var htmlSerializer = function (type, element, content, children) {
  var serializeEmbed = function(element) {
    var html = element.oembed.html.replace('src','data-src');
    return (`
      <div data-oembed="${element.oembed.embed_url}" data-oembed-type="${element.oembed.type}" data-oembed-provider="${element.oembed.provider_name}" class="lazy-load">
          ${html}
      </div>
    `);
  }

  var getImageHtml = function(curEl){
    console.log("IMG element")
    if(curEl.linkTo){
      var el = (curEl.alt) ? '<a href="'+ curEl.linkTo.url + '" target="_blank"> <img class="lazy-load" data-src="' + curEl.url + '" alt="' + curEl.alt + '" title="' + curEl.alt + '"/></a>' : '<a href="'+ curEl.linkTo.url + '"  target="_blank"><img  class="lazy-load" data-src="' + curEl.url + '"/></a>';
      return el;
    }else{
      return (curEl.alt) ? '<img class="lazy-load" data-src="' + curEl.url + '" alt="' + curEl.alt + '" title="' + curEl.alt + '"/>' : '<img  class="lazy-load" data-src="' + curEl.url + '"/>';
    }
  }
  switch(type) {

    // Add a class to paragraph elements
    case Elements.paragraph:
      return '<p class="paragraph-class">' + children.join('') + '</p>';

    // Don't wrap images in a <p> tag
    case Elements.image:
      return getImageHtml(element)

    case Elements.embed:
      console.log("embed element:::::::")
      console.log(element)
      return serializeEmbed(element)

    // case Elements.heading3:
    //   return `<h3 id="${encodeURI(children.join('').replace(/\s+/g,'-'))}">${children.join('')}</h3>`
    // Return null to stick with the default behavior
    default:
      return null;
  }
};

/**
linkTo:
11:59:08 web.1         |     { link_type: 'Web',
11:59:08 web.1         |       url: 'https://www.circuito.io/app?components=513%2C13879%2C417986%2C11286&utm_source=blog&utm_campaign=hardent' } }


**/

module.exports = {
  getSocialLink,
  getDate,
  getFirstParagraph,
  getPostDate,
  htmlSerializer,
  pugHtmlSerializer,
  getAmpLink,
};
